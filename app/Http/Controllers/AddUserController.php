<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use \App\Model\AddUserModel;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use \App\Utils\DBUtils;
use Log;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\Response;
use Maatwebsite\Excel\Facades\Excel;
use GuzzleHttp\Client;
class AddUserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /*
     * If you use Excel 2007 or upper set content type to
     *  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" and file extension to xlsx
     */


    public function exportXLS(){
        $filename=request("filename");
        $filetype=request("filetype");

        /*
// Send a request to https://foo.com/api/test
        $response = $client->request('GET', 'test');
// Send a request to https://foo.com/root
        $response = $client->request('GET', '/root');
        */

        //$contents = (string) $stream;
        //Excel::create('Filename')->export('xls');
        /*

        $response = new Response();
        $response->setContent($stream);

// the headers public attribute is a ResponseHeaderBag
        $response->headers->set('Content-Type', 'application/vnd.ms-excel');
        $response->headers->set('Content-Disposition', 'attachment; filename="export.xls"');
        */
        /*
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Hello')
            ->setCellValue('B2', 'world!')
            ->setCellValue('C1', 'Hello')
            ->setCellValue('D2', 'world!');
       // Log::info($objPHPExcel);
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="01simple.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        */
        $path=date('Y_m_d');
        Log::info($path);
        $name="exportData.".$filetype;
        $pathToFile="/Users/imake/PhpstormProjects/git_project/ais_v3/public/exportData/".$path."/".$filename.".".$filetype;
        //$pathToFile="/Users/imake/Desktop/exportData/2016_06_30/5npx2w5xxla88hrx3z81ttog3ul5jqd9c48w.ods";
        // $pathToFile="/Users/imake/Desktop/ocm.xls";
        //$name="omc.xls";
        $headers = array(
            'Content-Type: application/vnd.ms-excel',
           // 'Content-Disposition: attachment;filename="'.$name.'"',
           // 'Content-Disposition: attachment;filename="omc.xls"',
        );
        Log::info($pathToFile);
       return  response()->download($pathToFile, $name, $headers);
        // exit;
      /*
        $response = new StreamedResponse(function(){
            // Open output stream
            $handle = fopen('php://output', 'w');
        $client = new Client(['base_uri' => 'http://localhost:3000/v1/']);
        // Send a request to https://foo.com/api/test
        $response_java = $client->request('GET', 'export', [
            // 'body' => $json_str
        ]);

       //  $data_result=$response->getBody();
        //return $data_result;
        $stream = $response_java->getBody();
            // fputs($handle, $stream);
        Log::info($stream);
            // Add CSV headers

            fputcsv($handle, [
                'id',
                'name',
                'email'
            ]);
            fputcsv($handle, [
                'id2',
                'name3',
                'email2'
            ]);

            // Get all users

            foreach (User::all() as $user) {
                // Add a new row with data
                fputcsv($handle, [
                    $user->id,
                    $user->name,
                    $user->email
                ]);
            }



            // Close the output stream
            fclose($handle);
        }, 200, [
            // 'Content-Type' => 'text/csv',
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'attachment; filename="export.xls"',
        ]);
*/
        /*
        Excel::create('Filename', function($excel) {

            // Set the title
    $excel->setTitle('Our new awesome title');

    // Chain the setters
    $excel->setCreator('Maatwebsite')
          ->setCompany('Maatwebsite');

    // Call them separately
    $excel->setDescription('A demonstration to change the file properties');

        })->export('xls');
*/
         //return $response;
        //return $response_java;
    }
    public function search()
    {
        Log::info("x dbName->y");
        $dbName=DBUtils::getDBName();
        Log::info("x dbName->".$dbName);
        $search = Input::get('search');
        $sortBy = Input::get('sortBy');
        $orderBy= Input::get('orderBy');
      //  $users = DB::connection('foo');
        // solution 1
        /*
        $someModel=new AddUserModel();
        $someModel->setConnection('mysql_ais_473');
        $datas = $someModel->newQuery();
        */
        // solution 2  ::on('connection_name')
        $datas=AddUserModel::query();

        if(Input::has('page')){ // paging
            Log::info("into paging");
            $search = session()->get('addUser_search');
            $sortBy = session()->get('sortBy');
            $orderBy= session()->get('orderBy');
        }
        if(!empty($search)){
            $datas= $datas->Where(function ($datas) use ($search){
                $datas->orWhere('A', 'LIKE', "%$search%")
                    ->orWhere('C', 'LIKE',"%$search%");
            });
        }
        if(!empty($sortBy) && !empty($orderBy)){
            $datas=$datas->orderBy($sortBy,$orderBy);
        }
        session()->put('sortBy',$sortBy);
        session()->put('orderBy',$orderBy);
        session()->put('addUser_search',$search);

       // $datas=$datas->paginate(10);
        $datas=$datas->orderBy('updated_at','DESC')->paginate(10);
        /*
        $info_employee = AddUserModel::orderBy('updated_at','DESC')
           // ->orderBy('updated_at','DESC')
            ->paginate(12);
*/
        return view('ais/addUser', ['info_employee'=>$datas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->input('empId');
        Log::info('into store  ['.$id.']');
        if($id!=null) {
            $emp = AddUserModel::find($id);
            $emp->A = $request->input('empNo');
            $emp->B = $request->input('empTitle');
            $emp->C = $request->input('empFirstName') . "   " . $request->input('empLastName');
            $emp->D0 = $request->input('empPriority');

            $emp->save();
            session()->flash('message', ' Update successfuly.');
        }else{
            //$users = DB::connection('mysql2')->select(...);
            //$maxId = DB::connection(DBUtils::getDBName())->table('mmemployee_table')->max('ZZ');
            $maxId = DB::table('mmemployee_table')->max('ZZ');
            $emp = new AddUserModel();
            $emp->ZZ = $maxId+1;
            $emp->A = $request->input('empNo');
            $emp->B = $request->input('empTitle');
            $emp->C = $request->input('empFirstName') . "   " . $request->input('empLastName');
            $emp->D0 = $request->input('empPriority');
            $emp->save();
            session()->flash('message', ' Save successfuly.');
        }
        return redirect('ais/addUser');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteSelect(Request $request){

//        AddUserModel::find(explode(',', $id));
//        alert($id);
    //$id=$request->input('empId');
        foreach($_GET['checkbox'] as $check) {
            //echo $check . ', ';
            AddUserModel::find($check)->delete();
        }
        session()->flash('message', 'Delete successfuly.');
        return redirect('ais/addUser');
    }

    public function destroy($id)
    {
        Log::info('into destroy  ['.$id.']');
        AddUserModel::find($id)->delete();
        session()->flash('message', ' Delete successfuly.');
        return redirect('ais/addUser');
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: imake
 * Date: 04/04/2016
 * Time: 11:49
 */

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use \App\Model\MmpointModel;
use Log;
use Illuminate\Support\Facades\Auth;
use \App\Utils\DBUtils;

class TagAjax extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function validateTag(Request $request){
        Log::info("Into postMmnam callAjax");
        $error_init = array();
        // do something
        $mode=request("mode");
        $type=request("type");
        Log::info("mode ".$mode);
        $count=0;
        if($type=='tagname'){
            $tagname=request("tagname");
            Log::info("tagname ".$tagname);
            if($mode=='add'){
                $count = DB::connection(DBUtils::getDBName())->table('mmtag_table')->where('B','=',$tagname)->count();
            }else if($mode=='edit'){
                $A=request("A");
                $count = DB::connection(DBUtils::getDBName())->table('mmtag_table')->where('B','=',$tagname)->where('A','<>',$A)->count();
            }
            if($count>0){
                $error_key = array();
                $error_key["key"] =  "tagDescription";
                array_push($error_init, $error_key);
            }
        }else if($type=='LPMB'){
            $values=request("values");
            Log::info($values);
            $valuesStr = json_encode($values);
            Log::info($valuesStr);

            $modalAddEditTag_user_mmplant=request("modalAddEditTag_user_mmplant");
            $names=[];
            if($modalAddEditTag_user_mmplant=='1'){
                $names=['04','05','06','07'];
            }else if($modalAddEditTag_user_mmplant=='2'){
                $names=['08','09','10','11','12','13'];
            }else if($modalAddEditTag_user_mmplant=='3'){
                $names=['08','09','10','11','12','13'];
            }
            $efgh_array = ['E','F','G','H'];



            //Log::info(sizeof($formulas_init));
            //$jsonStr = json_encode($formulas_init);

            for( $i=0;$i<sizeof($values);$i++){
                $query_name_str=" SELECT count(*) as count FROM mmtag_table where ";

                $query_str=" SELECT count(*) as count FROM mmtag_table where ";
                Log::info($values[$i]["name"]);
                Log::info($values[$i]["value"]);
                $query_name_str=$query_name_str."  C".$values[$i]["name"]."='".$values[$i]["value"]."' ";
                $value_size=sizeof($values[$i]["key"]);
                for( $j=0;$j<$value_size;$j++){
                    $query_str=$query_str." ".$efgh_array[$j].$values[$i]["name"]."='".$values[$i]["key"][$j]."'";
                     if($j!=$value_size-1){
                         $query_str=$query_str.' and ';
                     }
                     Log::info(" ".$values[$i]["key"][$j]);
                }
                //$('#mm04L').val('');
               // $query=" SELECT count(*) as count FROM mmtag_table where E4='4' and F4='2' and G4='3' and H4='234' ";
                if($mode=='edit'){
                    $A=request("A");
                    $query_name_str=$query_name_str." and A !=".$A;
                    $query_str=$query_str." and A !=".$A;
                }
                Log::info(" query_name_str =".$query_name_str);
                Log::info(" query_str =".$query_str);
                $resultSet = DB::connection(DBUtils::getDBName())->select($query_name_str);
                $count_inner=$resultSet[0]->count;
                if($count_inner>0){
                    $error_key = array();
                    $error_key["key"] =  "tag".$values[$i]["name"];
                    array_push($error_init, $error_key);
                }
                $count=$count+$count_inner;

                $resultSet = DB::connection(DBUtils::getDBName())->select($query_str);
                $count_inner=$resultSet[0]->count;
                if($count_inner>0){
                    $error_key = array();
                    $keyVal=$values[$i]["name"];
                    if(sizeof($values[$i]["name"])==1){
                        $keyVal="0".$keyVal;
                    }
                    $error_key["key"] =  "mm".$keyVal;
                    array_push($error_init, $error_key);
                }
                $count=$count+$count_inner;
            }

        }

        return response()->json(['count'=>json_encode($count),'error_init'=>json_encode($error_init)]);
    }
}
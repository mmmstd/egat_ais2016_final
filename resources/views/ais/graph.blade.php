@include('layouts.script')
@section('page_title','Trend')
@extends('layouts.main')
@include('layouts.navigation')

@section('body')
    @include('layouts.header')
@section('content')
    <!-- Content Start-->
    <link href="/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="/css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link href="/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

    <link href="/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
    <!-- Data picker -->
    <script src="/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <!-- Clock picker -->
    <script src="/js/plugins/clockpicker/clockpicker.js"></script>
    <!--
    <script src="/js/plugins/iCheck/icheck.min.js"></script>
    -->
    <!-- iCheck -->
    <script src='/Controller/cMain.js'></script>
    <script src='/Controller/cGraph.js'></script>
    <link href="/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">


    <link href="/css/graph.css" rel="stylesheet">
    <!-- Main title Start
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Design Trend </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index">Design</a>
                </li>
                <li class="active">
                    <strong>Design Trend</strong>
                </li>
            </ol>
        </div>
    </div>
    -->
    <!-- Main title end -->
    <div class='row'>
        <div id="graphElement" style="display:none" class="col-lg-12">

            <!--
            <button type="button" onclick="closeGraph()"  class="btn btn-danger  btn-sm exitTrendPoint">
                <i class="fa fa-times-circle"></i>
            </button>
            -->
            <div class='row'>

                <!--
                <label class='font-noraml'>Range select</label>
                -->
                <label class='col-lg-1 control-label textAlign' style="padding-top:5px">ช่วงเวลา<!-- Scale Time  --></label>
                <div class='col-lg-2 scaleTimeR'>

                <select name='scaleTime' id='scaleTime' style="width:150px;height: 29px" class='form-control input-sm ' onchange="displayElementScaleTime(this)">
                    <!-- -->
                    <option value='month'>Month</option>
                    <option value='day'>Day</option>

                    <option value='hour' >Hour</option>
                    <!-- -->
                    <option value='minute'  selected='selected'>Minute</option>
                    <option value='second' >Second</option>

                </select>
                </div>
                <div class='col-lg-4' id='datepicker_twoselect' style="display:none">
                    <div class='form-group' id='date_month' style="display:none">
                        <div class='input-daterange input-group' id='datepicker_month'>
                            <span class='input-group-addon'>เดือน</span>
                            <input type='text' class='input-sm form-control' style="cursor:pointer;background-color: #f6f6f6" readonly="readonly" id="startMonthTime"  />
                            <span class='input-group-addon'>ถึงเดือน</span>
                            <input type='text' class='input-sm form-control' style="cursor:pointer;background-color: #f6f6f6" readonly="readonly" id="endMonthTime"  />
                        </div>
                    </div>
                    <div class='form-group' id='date_day' style="display:block">
                        <div class='input-daterange input-group' id='datepicker_day'>
                        <span class='input-group-addon'>วันที่</span>
                        <input type='text' class='input-sm form-control' style="cursor:pointer;background-color: #f6f6f6" readonly="readonly" id="startTime" />
                        <span class='input-group-addon'>ถึงวันที่</span>
                        <input type='text' class='input-sm form-control' style="cursor:pointer;background-color: #f6f6f6" readonly="readonly" id="endTime" />
                        </div>
                    </div>
                    <div class='form-group' id='date_hr' style="display:block">
                        <div class='input-daterange input-group' id='datepicker_HR'>
                            <span class='input-group-addon'>วันที่</span>
                            <input type='text' class='input-sm form-control' style="cursor:pointer;background-color: #f6f6f6" readonly="readonly" id="startHRTime" />
                            <span class='input-group-addon'>ถึงวันที่</span>
                            <input type='text' class='input-sm form-control' style="cursor:pointer;background-color: #f6f6f6" readonly="readonly" id="endHRTime" />
                        </div>
                    </div>
                </div>
                <div class='col-lg-2'  id='datepicker_oneselect'  style="display:block">
                    <div class='form-group' id='date_one'>
                        <div class='input-daterange input-group' id='datepicker_onetime'>
                            <span class='input-group-addon'>วันที่</span>
                            <input type='text' class='input-sm form-control' style="cursor:pointer;background-color: #f6f6f6" readonly="readonly" id="oneTime"  />
                        </div>
                    </div>
                </div>
                <div id="showHis" style="display:none">
                <label class='col-lg-1 control-label textAlign' style="padding-top:5px">ชั่วโมง:นาที</label>
                <div class='col-lg-1'>
                    <div class="input-group clockpicker" data-autoclose="true">
                        <input type="text" id="timeHis" readonly="readonly" style="cursor:pointer;background-color: #f6f6f6;width:45px;height:25px" class="form-control" value="" >
                    </div>
                </div>
                </div>
                <div class='col-lg-1'>
                    <button  id='btnScaleTime' onclick="refreshChart()" class='btn btn-sm btn-primary'>ดูข้อมูล</button>
                </div>


            </div>
            <div id="trendContentArea" class="">


                <div class="row scaleTimeMenu ">
                    <div class="col-md-2 ">
                        <!--<div class='col-md-2 basicSlideArea'>
                         slide start -->
                        <div class="scaleTimeMenuRightArea" style="display:block" id="scaleTimeMenuRightArea">
                            <!-- <div id="keypress" ></div> -->
                            <!-- start -->
                            <div class="setTimeCustomArea timeFocusExpand">
                                <div class="doubleLeftArea">

                                    <a class="btn btn-white btn-bitbucket btn-sm  " onclick="changeSpanTime('minus')" id="minusSpanTime">
                                        <i class="fa fa-minus"></i>
                                    </a>
                                </div>


                                <div class="textExpandFocus">
                                    <input type="text" class="form-control input-sm expandFocus " id="expandFocus"/>
                                    <input type="hidden"  id="scaleTimeMinuteExpand"/>
                                    <input type="hidden"  id="scaleTimeSecondExpand"/>
                                </div>



                                <div class="doublerightArea">
                                    <a class="btn btn-white btn-bitbucket btn-sm  " onclick="changeSpanTime('plus')" id="plusSpanTime">
                                        <i class="fa fa-plus"></i>
                                    </a>

                                </div>

                            </div>
                            <!-- end -->
                        </div>
                        <div class="scaleTimeMenuLeftArea" style="display:block" id="scaleTimeMenuLeftArea">
                            4 Hour
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="btn btn-sm titleDate btnScaleTimeArea clicked">
                            <div id="scaleDateTimeArea">
                            <!--     ข้อมูลวันที่ 20 มิถุนายน 2559 - 20 มิถุนายน 2559  &nbsp;&nbsp; -->
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="col-md-5">
                        <div class="btn btn-sm titleDate btnScaleTimeArea clicked" id="btnScaleTimeArea-3195" data-container="body" data-toggle="popover" data-placement="bottom" title="" data-html="true" data-content="
                    <div class='container widthScaleTime' >
                     <div class='row rowTopBottomMargin'>

                           <div class='form-group'>
                                <label class='col-lg-3 control-label textAlign'>ช่วงเวลา</label>
                                <div class='col-lg-9 scaleTimeR'>
                                    <select name='scaleTime-3195' id='scaleTime-3195' class='form-control input-sm '>
                                        <option value='Month'>Month</option>
                                        <option value='Day'>Day</option>
                                        <option value='Hour'>Hour</option>
                                        <option value='Minute' selected='selected'>Minute</option>
                                        <option value='Second' >Second</option>
                                    </select>
                                </div>
                           </div>
                     </div>
                     <div class='row rowTopBottomMargin'>
                           <div class='form-group' >
                                <label class='col-lg-3 control-label textAlign'>วันที่</label>
                                <div class='col-lg-9 scaleTimeR' id='dateFromArea-3195'>
                                       <input type='text' id='dateFrom-3195' value='' class='form-control input-sm' style='width: 100%'>
                                </div>
                           </div>
                     </div>
                     <div class='row rowTopBottomMargin forSecondFormHide'>
                            <div class='form-group' >
                                <label class='col-lg-3 control-label textAlign'>ถึงวันที่</label>
                                <div class='col-lg-9 scaleTimeR' id='dateToArea-3195'>
                                       <input type='text' id='dateTo-3195' value='' class='form-control input-sm' style='width: 100%'>
                                </div>
                           </div>
                     </div>
                       <div class='row rowTopBottomMargin forSecondFormShow displaynone'>
                            <div class='form-group' >
                                <label class='col-lg-3 control-label textAlign'>ชั่วโมง:นาที</label>
                                <div class='col-lg-9 scaleTimeR'>
                                       <input type='text' id='hour-3195' value='15:47' class='form-control input-sm '>
                                </div>
                           </div>
                     </div>
                     <div class='row rowTopBottomMargin forSecondFormShow displaynone'>
                            <div class='form-group' >
                                <label class='col-lg-3 control-label textAlign'>นาที</label>
                                <div class='col-lg-9 scaleTimeR'>
                                       <input type='text' id='minute-3195' value='02' class='form-control input-sm '>
                                </div>
                           </div>
                     </div>
                     <div class='btnScaleArea'>
                        <div class='pull-right'>
                            <button  id='btnScaleTime-3195' class='btn btn-sm btn-primary btnScaleTime'>OK</button>
                            <button  class='btn btn-sm btn-white btnScaleTimeCancel' id='btnScaleTimeCancel-3195'>Cancel</button>
                        </div>
                     </div>
                    </div>
                        " data-original-title="<i class='fa fa-calendar'></i> Scale Times">
                            <div id="scaleDateTimeArea-3195">ข้อมูลวันที่ 20 มิถุนายน 2559 - 20 มิถุนายน 2559  &nbsp;&nbsp;<i style="font-size:16px;" class="fa fa-calendar"></i></div>
                        </div>

                    </div>
                    -->
                    <div class="col-md-5">
                        <!-- date display -->
                        <div class="downloadSettingArea">
                            <button type="button" id="downloadSettingArea" class="btn btn-primary btn-sm  " data-container="body" data-toggle="popover" data-placement="bottom" data-html="true" data-content="
                        <table>
                            <tr>
                                <td>
                                    <input type='radio' name='download' value='xls'>
                                </td>
                                <td>
                                    &nbsp;Excel
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type='radio' name='download' value='ods'>
                                </td>
                                <td>
                                    &nbsp;Libre Office
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type='radio' name='download' value='csv'>
                                </td>
                                <td>
                                    &nbsp;CSV
                                </td>
                            </tr>
                            <tr>
                                <td colspan='2'>
                                    <button class='btn btn-primary  btn-xs ' onclick='downloadDataFile()' id='downloadData' type='button'>
                                    <i class='fa fa-download'></i>
                                        Download
                                    </button>

                                    <button class='btn btn-white btn-xs' onclick='closepopover();' id='cancelDownloadData' type='button'>
                                       Cancel
                                    </button>
                                </td>
                            </tr>

                        </table>



                        " data-original-title="" title="">
                                <i class="fa fa-download"></i>
                            </button>
                            <!--
                            <button type="button" data-toggle="modal" data-target="#editTrendPointModal" id="editTrendPoint-3195" class="btn btn-warning  btn-sm  editTrendPoint">
                                <i class="fa fa-cogs"></i>
                            </button>
                            -->

                            <button type="button" onclick="redrawChart()" class="btn btn-success  btn-sm">
                                <i class="fa fa-refresh"></i>
                            </button>
                            <button type="button" onclick="closeGraph()" class="btn btn-danger  btn-sm exitTrendPoint">
                                <i class="fa fa-times-circle"></i>
                            </button>
                        </div>



                        <div id="timeExpend" style="display:block" class="setTimeCustomArea setTimeCustomAreaSecondHidden">
                            <div class="doubleLeftArea">
                                <a class="btn btn-sm btn-white btn-bitbucket   " onclick="changeStartTime('min')" id="reduceDay">
                                    <i class="fa fa-angle-double-left   "></i>
                                </a>
                                <a class="btn btn-white btn-bitbucket btn-sm  " onclick="changeStartTime('minus')" id="reduceStartTime">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                            </div>
                            <!-- input form start -->
                            <div class="textSetTime clockpicker" data-autoclose="true">
                                <input type="text" readonly="readonly" style="padding-left:10px;cursor:pointer;background-color: #f6f6f6;"
                                       class="form-control input-sm  startTimeForDisplay" id="startTimeForDisplay" placeholder="00:00">
                            </div>
                            <!-- input form end -->


                            <div class="doublerightArea">
                                <a class="btn btn-white btn-bitbucket btn-sm  " onclick="changeStartTime('plus')"  id="increaseStartTime">
                                    <i class="fa fa-angle-right"></i>
                                </a>

                                <a class="btn btn-white btn-bitbucket btn-sm  " onclick="changeStartTime('max')"  id="increaseDay">
                                    <i class="fa fa-angle-double-right"></i>
                                </a>
                            </div>

                        </div>
                        <!-- date display -->
                    </div>
                </div>

                <!-- kendo ui chart start -->
                <div class="row grachArea">

                    <div class="col-md-9 col-padding0 " id="boxLeft">

                        <div class="demo-section k-content wide " id="trendChartArea-3195">
                            <!--
                            <div id="chart" class="heightChart" style="width:770px" >

                            </div>
                              -->
                            <div id="chart" class="heightChart" style="background: center no-repeat url('/js/kendoCommercial/bg/world-map.png');" >

                            </div>


                            <!--
                            <div id="trendChart-3195" class="heightChart k-chart"
                                 style="position: relative; background: url(&quot;/js/kendoUI/bg/world-map.png&quot;) center center no-repeat;" data-role="chart">
                                 -->
                               <!--
                                <div class="k-tooltip" style="position: absolute; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Arial, Helvetica, sans-serif; border: 1px solid rgb(216, 150, 43); opacity: 1; top: 87px; left: 49px; display: none; background-color: rgb(216, 150, 43);">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <th colspan="2">10:20:00  น.</th>
                                        </tr>
                                        <tr>
                                            <td>U04D1:</td> <td>undefined</td></tr><tr><td>DC594:</td> <td>undefined</td></tr><tr><td>U05D3:</td> <td>undefined</td></tr><tr><td>DC599:</td> <td>undefined</td></tr><tr><td>DC600:</td> <td>undefined</td></tr></tbody></table>
                                </div>
                                -->
                            <!--
                            </div>
                            -->
                        </div>
                    </div>

                    <div class="col-md-3 col-padding0 " id="boxRight">
                        <div id="titleDateTime">
                            <div id="dateInDataDisplayAreaMenute-3195" class="dateInDataDisplayArea ">
                                <div class="dateInDataDisplay">
                                    <span class="dateInDataDisplayLeft" id="dateInDataDisplay"></span>
                                    <!--
                                    <span class="dateInDataDisplayRight" id="timeInDataDisplay">เวลา 10:20:00  น.</span>
                                    -->
                                </div>
                            </div>
                            <div id="dateInDataDisplayAreaHour-3195" class="dateInDataDisplayArea  displaynone">

                                <div id="dateTimeInDataDisplayHour-3195"></div>
                            </div>
                            <div id="dateInDataDisplayAreaHourDay-3195" class="dateInDataDisplayArea  displaynone">

                                <div id="dateTimeInDataDisplayDay-3195"></div>
                            </div>
                            <div id="dateInDataDisplayAreaMonth-3195" class="dateInDataDisplayArea  displaynone">

                                <div id="dateTimeInDataDisplayMonth-3195"></div>

                            </div>
                            <div id="dateInDataDisplayAreaSecond-3195" class="dateInDataDisplayArea  displaynone">

                                <div id="dateTimeInDataDisplaySecond-3195"></div>
                            </div>
                        </div>
                        <!-- list point area -->
                        <ul class="list-group clear-list m-t" id="listPointLeftArea">
                            <!-- list point start-->
                            <!--
                            <li class="list-group-item ">
                                <div class="row">
                                    <div class="col-md-10 col-sm-10 col-xs-10 ">
                                        <div class="row ">
                                            <div class="col-md-12  col-sm-12 col-xs-12" id="btnPoint-0">
                                                <div id="labelTitle">

                                                    <button id="U04D1" type="button" style="background: #d8962b;color:white;" class="btn  btn-xs btnSetingPoint btnSetingPoint-1" data-container="body" data-toggle="popover" data-placement="bottom" title="" data-html="true" data-content="


                                <table class=''>
                                    <tr>
                                        <td colspan='2'>
                                            <input type='checkbox' name='pointEvent' class='pointEvent i-checks' id='event-U04D1-3195' value='event'>&nbsp;Event
                                            &nbsp; <input type='checkbox' class='pointEvent i-checks' name='pointEvent' id='action-U04D1-3195' value='action'>&nbsp;Action
                                            &nbsp;<input type='checkbox' class='pointEvent i-checks' name='pointEvent' id='vpser-U04D1-3195' value='vpser'>&nbsp;VPSER
                                        </td>

                                    </tr>

                                    <tr class='btnArea' >
                                        <td colspan='2' >
                                            <div style='padding:5px;'>

                                            <button class='btn btn-primary btn-xs  btnOkSetingPoint' id='psOk-U04D1' type='button'>

                                               OK
                                            </button>
                                             <button class='btn btn-white btn-xs  btnCancelSetingPoint' id='psCancel-U04D1' type='button'>

                                               Cancel
                                            </button>
                                            </div>
                                        </td>
                                    </tr>

                                </table>



                                " data-original-title="Event Seting">
                                                        <i class="fa fa-gear"></i>
                                                    </button>
                             <span style="color:#d8962b">

                             <span class="clickHideShowPoint showPoint" id="showPoint-U04D1-3195">
                              <div class="pointName pointName-3195">
                             U04 LP Bypass Valve#1 Position                             </div>
                              <div class="pointTag pointTag-3195">,U04 40SF61C502</div>

                             <div class="pointId pointId-3195">,U04D1</div>
                             <div class="pointId2 pointId2-3195">,D1-4-92547</div>

                             </span>

                             </span>

                                                </div>
                                                <div id="setEvent">


                                                </div>


                                            </div>



                                        </div>
                                        <div class="row">

                                            <div class="col-md-12 col-sm-12 col-xs-12">

                                                <span class="pull-left valuePoint" id="valuePoint-U04D1-3195" style="color:#d8962b">1.35</span>
                                                <span class="pull-right planPoint" id="planPoint-U04D1-3195" style="color:#d8962b">1.00</span>

                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-2 fontSize10 col-sm-2 col-xs-2 col-padding0 unitWidth">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">%</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">100</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">0</div>
                                        </div>

                                    </div>
                                </div>
                            </li>
                            <div style="display: none;">

                                <div class="paramEmbedArea" id="paramEmbedArea-U04D1-3195">

                                </div>
                            </div>
                            -->
                            <!-- list point end-->

                            <!-- list point start-->
                            <!--
                            <li class="list-group-item ">
                                <div class="row">
                                    <div class="col-md-10 col-sm-10 col-xs-10 ">
                                        <div class="row ">
                                            <div class="col-md-12  col-sm-12 col-xs-12" id="btnPoint-2">
                                                <div id="labelTitle">
                                                    <button id="U05D3" type="button" style="background: #ea0202;color:white;" class="btn  btn-xs btnSetingPoint btnSetingPoint-3" data-container="body" data-toggle="popover" data-placement="bottom" title="" data-html="true" data-content="
                                <table class=''>
                                    <tr>
                                        <td colspan='2'>
                                            <input type='checkbox' name='pointEvent' class='pointEvent i-checks' id='event-U05D3-3195' value='event'>&nbsp;Event
                                            &nbsp; <input type='checkbox' class='pointEvent i-checks' name='pointEvent' id='action-U05D3-3195' value='action'>&nbsp;Action
                                            &nbsp;<input type='checkbox' class='pointEvent i-checks' name='pointEvent' id='vpser-U05D3-3195' value='vpser'>&nbsp;VPSER
                                        </td>

                                    </tr>
                                    <tr class='btnArea' >
                                        <td colspan='2' >
                                            <div style='padding:5px;'>
                                            <button class='btn btn-primary btn-xs  btnOkSetingPoint' id='psOk-U05D3' type='button'>
                                               OK
                                            </button>
                                             <button class='btn btn-white btn-xs  btnCancelSetingPoint' id='psCancel-U05D3' type='button'>
                                               Cancel
                                            </button>
                                            </div>
                                        </td>
                                    </tr>

                                </table>



                                " data-original-title="Event Seting">
                                                        <i class="fa fa-gear"></i>
                                                    </button>
                             <span style="color:#ea0202">

                             <span class="clickHideShowPoint showPoint" id="showPoint-U05D3-3195">
                              <div class="pointName pointName-3195">
                             U05 Aux.steam header pressure                             </div>
                              <div class="pointTag pointTag-3195">,U05 50RS70P001</div>
                             <div class="pointId pointId-3195">,U05D3</div>
                             <div class="pointId2 pointId2-3195">,D3-5-92549</div>
                             </span>
                             </span>

                                                </div>
                                                <div id="setEvent">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <span class="pull-left valuePoint" id="valuePoint-U05D3-3195" style="color:#ea0202">15.13</span>
                                                <span class="pull-right planPoint" id="planPoint-U05D3-3195" style="color:#ea0202">15.00</span>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-2 fontSize10 col-sm-2 col-xs-2 col-padding0 unitWidth">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">Bar</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">25</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">0</div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <div style="display: none;">
                                <div class="paramEmbedArea" id="paramEmbedArea-U05D3-3195">
                                </div>
                            </div>
                            -->
                            <!-- list point end-->
                        </ul>
                        <!-- list point area -->
                    </div>
                </div>

                <!-- kendo ui chart end -->
                <div class="row footGrachArea">
                    <input type="hidden" id="showByValue" value="showbyPointName">
                    <div class="col-sm-12">
                        <div class="trendFooterArea">
                            <div class="checkboxFooter">
                                <div class="i-checks">
                                    <label class="">
                                        <div id="showbyPointName" class="iradio_square-green checked" style="position: relative;">
                                            <div  class="iradio_square-green" onclick="changeNameShow('showbyPointName')" style="position: relative;">
                                                <input type="radio" value="showbyPointName" style="position: absolute; opacity: 0;">
                                                <!--
                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
                                                -->
                                            </div>
                                            <!--
                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                            -->
                                        </div>
                                        <i></i> Show by Point Name </label>
                                </div>
                            </div>
                            <div class="checkboxFooter">
                                <div class="i-checks">
                                    <label class="">
                                        <div id="showbyTagName" class="iradio_square-green" style="position: relative;">
                                            <div   class="iradio_square-green" onclick="changeNameShow('showbyTagName')" style="position: relative;">
                                                <input type="radio" class="showTrendby"  value="showbyTagName" style="position: absolute; opacity: 0;">
                                                <!--
                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
                                                -->
                                            </div>
                                            <!--
                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins>
                                            -->
                                        </div>
                                        <i></i> Show by Tag Name
                                    </label>
                                </div>
                                <!--
                                <div class="iradio_square-green" style="position: relative;"><label> <input type="radio" value="option1" name="a"/> <i></i> Option one </label></div>
                                <div class="iradio_square-green" style="position: relative;"><label> <input type="radio" checked="" value="option2" name="a"/> <i></i> Option two checked </label></div>
                                -->
                            </div>
                        </div>
                    </div>
                </div>
                <!--
                <div class="row footGrachArea">
                    <div class="col-sm-12">
                        <div class="trendFooterArea">
                            <div class="checkboxFooter">
                                <div class="i-checks">
                                    <label class="">
                                        <div class="iradio_square-green " style="position: relative;"><div class="iradio_square-green checked" style="position: relative;"><input type="radio" class="showTrendby" id="showTrendby-3195" name="showTrendby-3195" value="showbyPointName" checked="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                        <i></i> Show by Point Name </label>
                                </div>
                            </div>
                            <div class="checkboxFooter">
                                <div class="i-checks">
                                    <label class="">
                                        <div class="iradio_square-green" style="position: relative;"><div class="iradio_square-green" style="position: relative;"><input type="radio" class="showTrendby" id="showTrendby-3195" name="showTrendby-3195" value="showbyTagName" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                        <i></i> Show by Tag Name
                                    </label>
                                </div>
                            </div>

                            <div class="checkboxFooter showPointAll">
                                <div class="i-checks">
                                    <label class="">
                                        <div class="iradio_square-green " style="position: relative;"><div class="iradio_square-green checked" style="position: relative;"><input type="radio" id="showHiddenPoint-3195" class="showHiddenPoint" name="showHiddenPoint-3195" value="showPointAll" checked="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                        <i></i> Show Point All  </label>
                                </div>
                            </div>
                            <div class="checkboxFooter">
                                <div class="i-checks">
                                    <label class="">
                                        <div class="iradio_square-green " style="position: relative;">

                                            <div class="iradio_square-green" style="position: relative;"><input type="radio" class="showHiddenPoint" id="showHiddenPoint-3195" name="showHiddenPoint-3195" value="hiddenPointAll" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>

                                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                        <i></i> Hidden Point All
                                    </label>
                                </div>


                            </div>

                        </div>
                    </div>
                </div>
                 -->

            </div>
        </div>
        <!-- Table trend start-->
        <div id="trendAndPointElement" class="col-lg-12">
            <div class="ibox float-e-margins">
                <!--
                <div class="ibox-title">
                    <h5>Design Trend Management</h5>
                </div>
                -->
                <div class="ibox-content">
                    <div class="row">

                        @if(session()->has('message'))
                            <div class="col-md-12">
                                <div class="alert alert-success" style="margin: 5px 0px; padding: 5px 3px;" role="alert">
                                    <i class="glyphicon glyphicon-ok-sign"></i> {{ session()->get('message') }}
                                </div>
                            </div>
                        @elseif(session()->has('error_message'))
                            <div class="col-md-12">
                                <div class="alert alert-danger" style="margin: 5px 0px; padding: 5px 3px;" role="alert">
                                    <i class="glyphicon glyphicon-remove-sign"></i>
                                    <b>{{ session()->get('error_message') }}</b>{{ session()->get('error_message2') }}
                                </div>
                            </div>
                        @endif
                        {!! Form::open(array('url'=> 'ais/graph')) !!}
                        <div class='col-md-12 bgParam'>
                            <div class="col-md-4" style="margin-top: 8px">
                                Trend Group:
                                <!--  btn -->
                                <input type="hidden" id="design_trend_B_hidden" value="{{session()->get('design_trend_B')}}"/>
                                <select id="design_trend_B" name="design_trend_B">
                                    @if(session()->get('user_priority')>=128)
                                        <option value="-1">All Trend</option>
                                    @endif
                                <!--
                                        <option value="empId">My Trend</option>
                                        -->
                                    @foreach($mmtrend_groups as $mmtrend_group)
                                        @if($mmtrend_group->B=='9')
                                            <option value="{{Auth::user()->empId}}">{{$mmtrend_group->group_name}}</option>
                                        @endif
                                        @if(session()->get('user_priority')>=128)
                                            @if($mmtrend_group->B!='9')
                                                <option value="{{$mmtrend_group->B}}">{{$mmtrend_group->group_name}}</option>
                                        @endif
                                    @endif
                                @endforeach
                                </select>

                            </div>
                            <div class="col-md-2">
                                <input type="text" name="search" class="form-control"
                                       placeholder="ค้นหา" value="{{session()->get('design_trend_search')}}">
                            </div>
                            <div class="col-md-3" style="margin-top: 8px;width: 190px">

                                Sort By:
                                <!--  btn -->

                                <input type="hidden" id="sortBy_hidden" value="{{session()->get('sortBy')}}"/>
                                <select id="sortBy" name="sortBy">
                                    <option value=""></option>
                                    <option value="A">Trend Name</option>
                                </select>

                            </div>
                            <div class="col-md-2" style="margin-top: 8px">
                                Order By:
                                <!--  btn -->

                                <input type="hidden" id="orderBy_hidden" value="{{session()->get('orderBy')}}"/>
                                <select id="orderBy" name="orderBy">
                                    <option value=""></option>
                                    <option value="ASC">ASC</option>
                                    <option value="DESC">DESC</option>
                                </select>

                            </div>


                            <div class="col-md-1" style="margin-top: 8px"><button class="btn btn-sm btn-primary pull-left m-t-n-xs"><strong>Search</strong></button></div>

                        </div>
                        {!! Form::close() !!}

                        <!--
                        <div class='col-md-12'>
                            <a onclick="displayMmname('add','0')"  class="btn btn-primary  btn-sm">Add Trend</a>
                            <a onclick="displayMmnameDelete('deleteAll','0')" class="btn btn-w-m btn-danger  btn-sm">Delete Trend</a>
                        </div>
                        -->
                    </div>
                    <div id="editable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="editable" class="table table-striped table-bordered table-hover  dataTable"
                                       role="grid" aria-describedby="editable_info">
                                    <thead>
                                    <tr role="row">
                                        <!--
                                        <th class="" tabindex="0" aria-controls="" rowspan="1" colspan="1"
                                            style="width: 0%;" aria-sort="" aria-label=""> <input type='checkbox' id="checkAll">
                                        </th>
                                        -->
                                        <th class="" tabindex="0" aria-controls="editable" rowspan="1" colspan="1"
                                            style="width: 90%;" aria-label="Browser: activate to sort column ascending">
                                            Trend Name
                                        </th>
                                        <th align="center" class="center" tabindex="0" aria-controls="editable" rowspan="1" colspan="1"
                                            style="width: 10%;align-self: center" aria-label="Platform(s): activate to sort column ascending">
                                            Action
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <input type="hidden" id="mmtrend_zz"/>
                                    @foreach($mmtrendsM as $mmtrendM)
                                        <tr class="gradeA odd" role="row">
                                            <!--
                                            <td class="sorting_1">
                                                <input type='checkbox' name="checkbox[]"
                                                       class="ck" data-id="checkbox"  value="{{-- $mmtrendM->ZZ --}}">
                                            </td>
                                            -->
                                            <td>{{ $mmtrendM->A }}</td>
                                            <!--  URL::to('/addUser/destroy',$info_emp->ZZ)  -->
                                            <!--  url('/addUser/destroy',$info_emp->ZZ)  -->
                                            <td align="center">
                                                <a onclick="showmmtrend('{{ $mmtrendM->ZZ }}')"
                                                   class="btn btn-primary  btn-xs">แสดง Point</a>
                                                <!--
                                                |
                                                <a id="btnEdit" onclick="displayMmname('edit','{{-- $mmtrendM->ZZ --}}')" class="btn btn-dropbox btn-xs"><i style="color: #47a447;" class="glyphicon glyphicon-edit"></i><span hidden id="id">{{-- $mmtrendM->ZZ --}}</span></a>|
                                                <a onclick="displayMmnameDelete('delete','{{-- $mmtrendM->ZZ --}}')"  class="btn btn-dropbox btn-xs"><i class="glyphicon glyphicon-trash text-danger"></i></a>
                                                -->
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div style="float: right;">
                                    {!!  $mmtrendsM->render() !!}
                                </div>
                            </div>
                        </div>
                        <!-- Table trend end-->

                        <!-- Table show point start-->
                        <input type="hidden" id="mmtrend_pageNo" name="mmtrend_pageNo" />
                        <input type="hidden" id="mmtrend_pageSize" name="mmtrend_pageSize" value="10" />
                        <div id="trend_element" class="col-lg-12" style="display:none" >
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">

                                    <h5 id="trend_element_header">
                                        <!--
                                        แสดง Point ของ Trend001
                                        -->
                                    </h5>
                                    <div style="margin-left:0px" class="col-md-1 lableDropdownList">MMPlant</div>
                                    <div class="col-md-7" style="margin-top:7px">
                                        <!--
                                        <select id="mmplant" class="form-control m-b">
                                        -->
                                        @if(Session::get('user_mmplant')=='1')
                                            <span>
                                                <input type="checkbox"  onchange="onChangeCheckMMPLANT(this)" name="mmplant_check" value="4"/>MM04
                                            </span>
                                            <span class="point_padding"  >
                                                <input type="checkbox"  onchange="onChangeCheckMMPLANT(this)" name="mmplant_check" value="5"/>MM05
                                            </span>
                                            <span class="point_padding"  >
                                                <input type="checkbox"  onchange="onChangeCheckMMPLANT(this)"  name="mmplant_check" value="6"/>MM06
                                            </span>
                                            <span class="point_padding"  >
                                                <input type="checkbox"  onchange="onChangeCheckMMPLANT(this)" name="mmplant_check" value="7"/>MM07
                                            </span>
                                        @endif
                                        @if(Session::get('user_mmplant')=='2')
                                            <span>
                                                <input type="checkbox"  onchange="onChangeCheckMMPLANT(this)" name="mmplant_check" value="8"/>MM08
                                            </span>
                                            <span class="point_padding"  >
                                                <input type="checkbox"  onchange="onChangeCheckMMPLANT(this)" name="mmplant_check" value="9"/>MM09
                                            </span>
                                            <span class="point_padding"  >
                                                <input type="checkbox"  onchange="onChangeCheckMMPLANT(this)"  name="mmplant_check" value="10"/>MM10
                                            </span>
                                            <span class="point_padding" >
                                                <input type="checkbox"  onchange="onChangeCheckMMPLANT(this)" name="mmplant_check" value="11"/>MM11
                                            </span>
                                            <span class="point_padding"  >
                                                <input type="checkbox"  onchange="onChangeCheckMMPLANT(this)" name="mmplant_check" value="12"/>MM12
                                            </span>
                                            <span class="point_padding"  >
                                                <input type="checkbox"  onchange="onChangeCheckMMPLANT(this)" name="mmplant_check" value="13"/>MM13
                                            </span>
                                        @endif
                                        @if(Session::get('user_mmplant')=='3')
                                            <span>
                                                <input type="checkbox"  onchange="onChangeCheckMMPLANT(this)" name="mmplant_check" value="8"/>MM08
                                            </span>
                                            <span class="point_padding"  >
                                                <input type="checkbox"  onchange="onChangeCheckMMPLANT(this)" name="mmplant_check" value="9"/>MM09
                                            </span>
                                            <span class="point_padding"  >
                                                <input type="checkbox"  onchange="onChangeCheckMMPLANT(this)"  name="mmplant_check" value="10"/>MM10
                                            </span>
                                            <span class="point_padding"  >
                                                <input type="checkbox"  onchange="onChangeCheckMMPLANT(this)" name="mmplant_check" value="11"/>MM11
                                            </span>
                                            <span class="point_padding" >
                                                <input type="checkbox"  onchange="onChangeCheckMMPLANT(this)" name="mmplant_check" value="12"/>MM12
                                            </span>
                                            <span class="point_padding"  >
                                                <input type="checkbox"  onchange="onChangeCheckMMPLANT(this)" name="mmplant_check" value="13"/>MM13
                                            </span>
                                        @endif
                                        <span class="point_padding"  >
                                                <input type="checkbox"  onchange="onChangeCheckPointCompare(this)" id="mmplant_check_point_compare" />Point Compare
                                            </span>
                                        <!--
                                        <select id="mmplant" style="margin-top:7px" onchange="onChangeMMPLANT()">

                                                <option value="">All</option>
                                                <option value="4">MM04</option>
                                                <option value="5">MM05</option>
                                                <option value="6">MM06</option>
                                                <option value="7">MM07</option>

                                        </select>
                                        -->
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div class="row">
                                        <!--
                                        <div class="col-md-6 ">
                                            <a onclick="displayMmtrend('add','0','0')"
                                               class="btn btn-primary  btn-sm">Add Point</a>
                                            <a onclick="displayMmtrendDelete('deleteAll','0')"  class="btn btn-w-m btn-danger  btn-sm">Delete Point</a>
                                        </div>
                                        -->
                                        <!--
                                        <div class="col-md-3 lableDropdownList">MMPlant</div>
                                        <div class="col-md-3">
                                            <select id="mmplant" class="form-control m-b">
                                                <option>All Calculation</option>
                                                <option>My Calcualtion</option>
                                                <option>MM04-MM07</option>
                                                <option>MM04</option>
                                                <option>MM05</option>
                                                <option>MM06</option>
                                                <option>MM07</option>
                                            </select>
                                        </div>
                                            -->
                                    </div>
                                    <div id="editable_wrapper"
                                         class="dataTables_wrapper form-inline dt-bootstrap">
                                        <div class="row">
                                            <div id="trend_element_table" class="col-sm-12 table-responsive">
                                                <table id="editable"
                                                       class="table table-striped table-bordered table-hover  dataTable"
                                                       role="grid" aria-describedby="editable_info">
                                                    <thead>
                                                    <tr role="row">
                                                        <th class="" tabindex="0" aria-controls=""
                                                            rowspan="1" colspan="1" style="width: 0%;"
                                                            aria-sort="" aria-label="">  <input type='checkbox' id="checkAll_inner">
                                                        </th>
                                                        <th class="" tabindex="0" aria-controls="editable"
                                                            rowspan="1" colspan="1" style="width: 10%;"
                                                            aria-label="Browser: activate to sort column ascending">
                                                            Point
                                                        </th>
                                                        <th class="" tabindex="0" aria-controls="editable"
                                                            rowspan="1" colspan="1" style="width: 5%;"
                                                            aria-label="Platform(s): activate to sort column ascending">
                                                            MM
                                                        </th>
                                                        <th class="" tabindex="0" aria-controls="editable"
                                                            rowspan="1" colspan="1" style="width: 35%;"
                                                            aria-label="Platform(s): activate to sort column ascending">
                                                            Point Name
                                                        </th>
                                                        <th class="" tabindex="0" aria-controls="editable"
                                                            rowspan="1" colspan="1" style="width: 15%;"
                                                            aria-label="Platform(s): activate to sort column ascending">
                                                            Tag Name
                                                        </th>
                                                        <th class="" tabindex="0" aria-controls="editable"
                                                            rowspan="1" colspan="1" style="width: 10%;"
                                                            aria-label="Platform(s): activate to sort column ascending">
                                                            Unit
                                                        </th>
                                                        <th class="" tabindex="0" aria-controls="editable"
                                                            rowspan="1" colspan="1" style="width: 5%;"
                                                            aria-label="Platform(s): activate to sort column ascending">
                                                            Max
                                                        </th>
                                                        <th class="" tabindex="0" aria-controls="editable"
                                                            rowspan="1" colspan="1" style="width: 5%;"
                                                            aria-label="Platform(s): activate to sort column ascending">
                                                            Min
                                                        </th>
                                                        <!--
                                                        <th class="" tabindex="0" aria-controls="editable" rowspan="1" colspan="1"
                                                            style="width: 10%;" aria-label="Platform(s): activate to sort column ascending">
                                                            Action
                                                        </th>
                                                        -->
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div id="paging_element" style="float: right;display: none;">


                                        </div>
                                        <br style="clear:both">
                                        <!--
                                        <div class="col-xs-12"><buton id="btnPlotGraph-3207"
                                                                      class="btn btn-primary  btn-sm pull-right btnPlotGraph">Plot Graph </buton></div>
                                                                      -->
                                        <!--
                                        <div id="trend_paging" style="float: right;">

                                        </div>
                                        -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br style='clear:both'>
                        <!-- Table show point end-->
                    </div>
                    <!-- Modal Start -->
                    <div aria-hidden="true" role="dialog" tabindex="-1" id="myModal2" class="modal inmodal in"
                         style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content animated flipInY">
                                <div class="modal-header">
                                    <button data-dismiss="modal" class="close" type="button"><span
                                                aria-hidden="true">×</span><span class="sr-only">Close</span>
                                    </button>
                                    <h5 id="mmname_tilte_section" class="modal-title"></h5>
                                    <input type="hidden" id="mmname_mode"/>
                                    <input type="hidden" id="mmname_zz"/>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal">
                                        <div id="trend_name_element" class="form-group">
                                            <label class="col-lg-2 control-label padding5">ชื่อ Trend</label>

                                            <div class="col-lg-10 padding5">
                                                <input type="text" id="mmname_a" name="mmname_a" class="form-control " placeholder="ชื่อ Trend">
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-2 control-label padding5">Trend
                                                Group</label>

                                            <div class="col-lg-10 padding5">
                                                <span id="mmtrend_group_select_element"></span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button data-dismiss="modal" class="btn btn-white" type="button">ยกเลิก</button>
                                    <button class="btn btn-primary" onclick="doActionMmname()" type="button"><span id="button_mode_section"/></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal End -->

                    <!-- Modal Add Point Start -->
                    <div aria-hidden="true" role="dialog" tabindex="-1" id="myModalAddPoint"
                         class="modal inmodal in" style="display: none;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content animated flipInY">
                                <div class="modal-header">
                                    <button data-dismiss="modal" class="close" type="button"><span
                                                aria-hidden="true">×</span><span class="sr-only">Close</span>
                                    </button>
                                    <h5 id="mmtrend_tilte_section" class="modal-title">
                                        <!--
                                        เพิ่ม Point ไปที่ Trend001
                                        -->
                                    </h5>

                                </div>
                                <div class="modal-body">
                                    <!-- Table show point start-->
                                    <div class="col-lg-12">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-content">
                                                <div class="row">
                                                    <input type="hidden" id="mmtrend_mode" />
                                                    <input type="hidden" id="mmtrend_point_zz" />
                                                    <input type="hidden" id="mmtrend_point_h" />

                                                    <input type="hidden" id="user_mmplant" value="{{Session::get('user_mmplant')}}"/>
                                                    <div class="col-md-3 lableDropdownList">MMPlant</div>
                                                    <div class="col-md-3">
                                                        <select id="mmtrend_table_B" class="form-control m-b" onclick="searchMmpoint(this.value)">
                                                            @if(Session::get('user_mmplant')=='1')
                                                                <option value="4">MM04</option>
                                                                <option value="5">MM05</option>
                                                                <option value="6">MM06</option>
                                                                <option value="7">MM07</option>
                                                                <option value="47">MM04-07</option>
                                                            @endif
                                                            @if(Session::get('user_mmplant')=='2')
                                                                <option value="8">MM08</option>
                                                                <option value="9">MM09</option>
                                                                <option value="10">MM10</option>
                                                                <option value="11">MM11</option>
                                                                <option value="12">MM12</option>
                                                                <option value="13">MM13</option>
                                                                <option value="813">MM08-13</option>
                                                            @endif
                                                            @if(Session::get('user_mmplant')=='3')
                                                                <option value="8">MM08</option>
                                                                <option value="9">MM09</option>
                                                                <option value="10">MM10</option>
                                                                <option value="11">MM11</option>
                                                                <option value="12">MM12</option>
                                                                <option value="13">MM13</option>
                                                                <option value="813">MM08-13</option>
                                                            @endif
                                                            <option value="0">My Calculation</option>
                                                            @if (session()->get('user_priority') >= 128)
                                                                <option value="1">Standard Calculation</option>
                                                            @endif
                                                            @if (session()->get('user_priority') >= 254)
                                                                <option value="-1">All Calculation</option>
                                                        @endif
                                                        <!-- -->
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3 ">
                                                        <input id="keyword" type="text" placeholder="ค้นหา"
                                                               class="form-control ">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <bunton  onclick="searchMmpoint('')" class='btn btn-primary  btn-sm' type="button">ค้นหา</bunton>
                                                    </div>
                                                </div>
                                                <div id="editable_wrapper"
                                                     class="dataTables_wrapper form-inline dt-bootstrap">
                                                    <div class="row">
                                                        <div id="point_list_section" class="col-sm-12 table-responsive">
                                                            <table id="editable"
                                                                   class="table table-striped table-bordered table-hover  dataTable"
                                                                   role="grid" aria-describedby="editable_info">
                                                                <thead>
                                                                <tr role="row">
                                                                    <th class="" tabindex="0" aria-controls=""
                                                                        rowspan="1" colspan="1" style="width: 0%;"
                                                                        aria-sort="" aria-label="">

                                                                    </th>
                                                                    <th class="" tabindex="0"
                                                                        aria-controls="editable" rowspan="1"
                                                                        colspan="1" style="width: 20%;"
                                                                        aria-label="Browser: activate to sort column ascending">
                                                                        Point Name
                                                                    </th>
                                                                    <th class="" tabindex="0"
                                                                        aria-controls="editable" rowspan="1"
                                                                        colspan="1" style="width: 13%;"
                                                                        aria-label="Platform(s): activate to sort column ascending">
                                                                        Tag4
                                                                    </th>
                                                                    <th class="" tabindex="0"
                                                                        aria-controls="editable" rowspan="1"
                                                                        colspan="1" style="width: 13%;"
                                                                        aria-label="Platform(s): activate to sort column ascending">
                                                                        Tag5
                                                                    </th>
                                                                    <th class="" tabindex="0"
                                                                        aria-controls="editable" rowspan="1"
                                                                        colspan="1" style="width: 13%;"
                                                                        aria-label="Platform(s): activate to sort column ascending">
                                                                        Tag6
                                                                    </th>
                                                                    <th class="" tabindex="0"
                                                                        aria-controls="editable" rowspan="1"
                                                                        colspan="1" style="width: 13%;"
                                                                        aria-label="Platform(s): activate to sort column ascending">
                                                                        Tag7
                                                                    </th>
                                                                    <th class="" tabindex="0"
                                                                        aria-controls="editable" rowspan="1"
                                                                        colspan="1" style="width: 5%;"
                                                                        aria-label="Platform(s): activate to sort column ascending">
                                                                        Unit
                                                                    </th>
                                                                    <th class="" tabindex="0"
                                                                        aria-controls="editable" rowspan="1"
                                                                        colspan="1" style="width: 5%;"
                                                                        aria-label="Platform(s): activate to sort column ascending">
                                                                        Max
                                                                    </th>
                                                                    <th class="" tabindex="0"
                                                                        aria-controls="editable" rowspan="1"
                                                                        colspan="1" style="width: 5%;"
                                                                        aria-label="Platform(s): activate to sort column ascending">
                                                                        Min
                                                                    </th>
                                                                    <th class="" tabindex="0"
                                                                        aria-controls="editable" rowspan="1"
                                                                        colspan="1" style="width: 5%;"
                                                                        aria-label="Platform(s): activate to sort column ascending">
                                                                        Data
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr class="gradeA odd" role="row">
                                                                    <td class="sorting_1">
                                                                        <input type="radio"  name="point_ids_input[]"
                                                                               class="i-checks">
                                                                    </td>
                                                                    <td>Point001</td>
                                                                    <td>40HF02U066</td>
                                                                    <td>50HF02U066</td>
                                                                    <td>60HF02U066</td>
                                                                    <td>70HF02U066</td>
                                                                    <td>N/A</td>
                                                                    <td>2</td>
                                                                    <td>0</td>
                                                                    <td>435</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class='col-md-2'>
                                                    <input id="mmpoint_table_G0" type="text" placeholder="Max" class="form-control ">
                                                </div>
                                                <div class='col-md-2'>
                                                    <input id="mmpoint_table_G1" type="text" placeholder="Min" class="form-control ">
                                                </div>
                                                <div class='col-md-1 lableText'>
                                                    หน่วยวัด:
                                                </div>
                                                <div class='col-md-2 lableText'>
                                                    <input id="mmpoint_table_F" readonly="readonly" type="text" placeholder="" class="form-control ">
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Table show point end-->
                                </div>
                                <div class="modal-footer" style="padding: 10px 5px;">
                                    <button data-dismiss="modal" class="btn btn-white" type="button">ยกเลิก</button>
                                    <button class="btn btn-primary" onclick="doActionMmtrend()" type="button"><span id="button_mmtrend_mode_section"/></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal Add Point End -->
                    <!-- Modal Delete  Start -->
                    <div aria-hidden="true" role="dialog" tabindex="-1" id="myModalDelete" class="modal inmodal in"
                         style="display: none;">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content animated flipInY">
                                <div class="modal-header">
                                    <button data-dismiss="modal" class="close" type="button"><span
                                                aria-hidden="true">×</span><span class="sr-only">Close</span>
                                    </button>
                                    <h5 id="mmname_tilte_section" class="modal-title">ต้องการลบข้อมูล ?</h5>
                                    <input type="hidden" id="mmtrend_group_b"/>
                                    <input type="hidden" id="mmtrend_group_mode"/>
                                </div>
                                <!--
                                -->
                                <div class="modal-footer">
                                    <button data-dismiss="modal" class="btn btn-white" type="button">ยกเลิก</button>
                                    <button class="btn btn-primary" onclick="doDeleteMmname()" type="button">ตกลง</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal Delete  End -->

                    <!-- Modal mmtrend Delete  Start -->
                    <div aria-hidden="true" role="dialog" tabindex="-1" id="myModalMmtrendDelete" class="modal inmodal in"
                         style="display: none;">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content animated flipInY">
                                <div class="modal-header">
                                    <button data-dismiss="modal" class="close" type="button"><span
                                                aria-hidden="true">×</span><span class="sr-only">Close</span>
                                    </button>
                                    <h5 id="mmname_tilte_section" class="modal-title">ต้องการลบข้อมูล ?</h5>
                                    <input type="hidden" id="mmtrend_table_zz"/>
                                    <input type="hidden" id="mmtrend_table_mode"/>
                                </div>
                                <!--
                                <div class="modal-body">

                                </div>
                                -->
                                <div class="modal-footer">
                                    <button data-dismiss="modal" class="btn btn-white" type="button">ยกเลิก</button>
                                    <button class="btn btn-primary" onclick="doDeleteMmtrend()" type="button">ตกลง</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal mmtrend Delete  End -->
                    <!-- Content End-->
@stop
@section('footer')
    @include('layouts.footer')
@stop
@stop